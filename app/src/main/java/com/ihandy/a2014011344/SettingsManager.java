package com.ihandy.a2014011344;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.ihandy.a2014011344.DB.ConfigEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dc on 9/8/16.
 * PreferenceManager and MultiSelectListPreference is too simple to suit our needs.
 * This class is a workaround.
 */
public class SettingsManager implements DB.DBDropListener {
    public static class SectionFilter {
        List<String> catIds, catNames, catsOn;
        SectionFilter() {
            catIds = new ArrayList<>();
            catNames = new ArrayList<>();
            catsOn = new ArrayList<>();
        }
    }

    public static class Settings {
        SectionFilter cn, en;
        Settings() {
            cn = new SectionFilter();
            en = new SectionFilter();
        }
    }

    public static synchronized SettingsManager getInstance(Context c) {
        if (instance == null) {
            instance = new SettingsManager(c);
        }
        return instance;
    }

    static SettingsManager instance;

    public static final String CFG_KEY = "conf";

    DB dbInstance;
    DataSource dataSource;
    Settings settings;

    @Override
    public void onDbDropped() {
        settings = new Settings();
    }

    private void flush(boolean isSync) {
        Observable<SQLiteDatabase> db_ = dbInstance.getWritableDB();
        if (! isSync) {
            db_ = db_.observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.computation());
        }
        db_.subscribe(db -> {
            synchronized (db) {
                ContentValues cv = new ContentValues();
                cv.put(DB.SQLGen.Table.COL_NAME_ID, 0);
                cv.put(ConfigEntry.COL_NAME_KEY, CFG_KEY);
                cv.put(ConfigEntry.COL_NAME_VALUE, new Gson().toJson(settings));
                db.replace(ConfigEntry.DB_NAME, null, cv);
            }
        }, e -> Log.e(TAG, "flush: ", e));
    }

    void initEntry() {
        settings = new Settings();
        flush(true);
    }

    SettingsManager(Context c) {
        dataSource = DataSource.getInstance(c);
        dbInstance = DB.i(c);
        // Start reading settings
        dbInstance.queryDB(ConfigEntry.DB_NAME, ConfigEntry.COL_NAME_KEY + "=?", new String[]{CFG_KEY})
                .subscribe(cursor -> {
                    if (cursor == null || cursor.getCount() == 0) {
                        initEntry();
                    } else {
                        cursor.moveToNext();
                        String s = cursor.getString(cursor.getColumnIndex(ConfigEntry.COL_NAME_VALUE));
                        settings = new Gson().fromJson(s, Settings.class);
                    }
                    if (cursor != null) cursor.close();
                }, e -> Log.e(TAG, "SettingsManager: ", e));
        dbInstance.addDbDropListener(this);
    }

    private static final String TAG = "SettingsManager";

    Observable<Void> syncCategoriesOnNull(NewsAPI.Locale locale) {
        if (getFilter(locale).catIds.size() > 0) {
            return Observable.just(null);
        }
        return dataSource.getAPI(locale).getCategories(Long.toString(System.currentTimeMillis()))
                .map(categories -> {
                    updateCategories(locale, categories);
                    return null;
                });
    }

    SectionFilter getFilter(NewsAPI.Locale locale) {
        return locale == NewsAPI.Locale.EN ? settings.en : settings.cn;
    }

    void updateFilters(Set<String> en, Set<String> cn) {
        if (en != null) {
            settings.en.catsOn.clear();
            settings.en.catsOn.addAll(en);
            settings.en.catsOn.retainAll(settings.en.catIds);
        }
        if (cn != null) {
            settings.cn.catsOn.clear();
            settings.cn.catsOn.addAll(cn);
            settings.cn.catsOn.retainAll(settings.cn.catIds);
        }
        flush(false);
    }
    void updateCategories(NewsAPI.Locale locale, List<NewsAPI.Category> newCats) {
        ArrayList<String> newIds = new ArrayList<>();
        for (NewsAPI.Category c: newCats) newIds.add(c.id);

        SectionFilter sf = locale == NewsAPI.Locale.CN ? settings.cn : settings.en;

        sf.catIds.retainAll(newIds);
        sf.catsOn.retainAll(newIds);
        newIds.removeAll(sf.catIds);
        sf.catIds.addAll(newIds);
        sf.catsOn.addAll(newIds);

        sf.catNames.clear();
        for (String id: sf.catIds) {
            String n = null;
            for (NewsAPI.Category c : newCats) {
                if (c.id.equals(id)) {
                    sf.catNames.add(n = c.name);
                    break;
                }
            }
            assert(n != null);
        }

        flush(false);
    }

}
