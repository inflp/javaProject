package com.ihandy.a2014011344;

import android.support.annotation.Nullable;
import android.util.Log;

import com.ihandy.a2014011344.NewsAPI.News;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by dc on 9/6/16.
 */
public class CnNews {
    private static final String TAG = "CnNews";
    static final String[] categoryNames =
            {"科技", "教育", "军事", "国内", "社会", "文化", "汽车", "国际", "体育", "财经", "健康", "娱乐"};

    private static String getIdForCat(String cat) {
        for (int i = 0; i < categoryNames.length; ++i) {
            if (categoryNames[i].equals(cat)) {
                return Integer.toString(i + 1);
            }
        }
        throw new IndexOutOfBoundsException("getIdForCat");
    }
    @Nullable
    static String getCatForId(String id) {
        try {
            return categoryNames[Integer.parseInt(id) - 1];
        } catch (Exception e) {
            return null;
        }
    }

    public static class CNNewsItem {
        String newsClassTag;
        String news_ID;
        String news_Source;
        String news_Title;
        String news_Time;
        String news_URL;
        String news_Author;
        String lang_Type;
        String news_Pictures;
        String news_Video;
        String news_Intro;
        News toNews() {
            try {
                News ret = new News();
                ret.category = getIdForCat(newsClassTag);
                long createTime = Long.parseLong(news_ID.substring(2, 12));
                long longId = ((long)news_ID.hashCode()) & 0xffffffffL;
                ret.news_id = (createTime * 1000000000L) + longId;
                ret.origin = news_Source;
                ret.source = new News.Source();
                ret.source.url = news_URL;
                ret.source.name = news_Source;
                ret.title = news_Title;
                ret.imgs = new ArrayList<>();
                if (news_Pictures != null) {
                    String[] imgs = news_Pictures.split(" ");
                    if (imgs.length > 0 && imgs[0].length() > 0) {
                        News.Url url = new News.Url();
                        url.url = imgs[0];
                        ret.imgs.add(url);
                    }
                }
                return ret;
            } catch (Exception e) {
                Log.w(TAG, "toNews: API changed again!", e);
                return null;
            }
        }
    }

    public static class CNNewsList {
        List<CNNewsItem> list;
        int pageNo;
        int pageSize;
        int totalPages;
        int totalRecords;
    }

    interface APIWrapper {
        @GET("latest")
        Observable<CNNewsList> getNewsByCategory(
                @Query("keyword") String keyword, @Query("pageNo") int pageNo,
                @Query("pageSize") int pageSize, @Query("category") String category);
    }

    public static class API implements NewsAPI {
        static String[] categoryIds;
        public static final int PAGE_SIZE = 10;

        HashMap<String, ArrayList<News>> newsCache;
        APIWrapper api_;

        API() {
            newsCache = new HashMap<>();
            if (categoryIds == null) {
                categoryIds = new String[categoryNames.length];
                for (int i = 0; i < categoryNames.length; ++i) {
                    categoryIds[i] = getIdForCat(categoryNames[i]);
                }
            }
            api_ = new Retrofit.Builder()
                    .baseUrl("http://166.111.68.66:2042/news/action/query/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build()
                    .create(APIWrapper.class);
        }

        @Override
        public Observable<List<Category>> getCategories(String timestamp) {
            ArrayList<Category> ret = new ArrayList<>();
            for (int i = 0; i < categoryNames.length; ++i) {
                ret.add(new Category(categoryIds[i], categoryNames[i]));
            }
            return Observable.just(ret);
        }

        synchronized void addCache(String id, ArrayList<News> newses) {
            if (! newsCache.containsKey(id))
                newsCache.put(id, newses);
        }

        @Override
        public Observable<List<News>> getNewsByCategory(String categoryId, String max_id) {
            // Check if categoryId is valid. May happen when user operates too fast.
            if (getCatForId(categoryId) == null) {
                Log.e(TAG, "getNewsByCategory: invalid category " + categoryId);
                return Observable.just(new ArrayList<>());
            }

            // Cache and get newses. No need to refresh cache since server hasn't updated for years.
            Observable<ArrayList<News>> ret;
            if (! newsCache.containsKey(categoryId)) {
                ret = api_.getNewsByCategory(getCatForId(categoryId), 1, 300, categoryId)
                        .map(list -> {
                            News arr[] = new News[list.list.size()];
                            int j = 0;
                            for (CNNewsItem i: list.list) {
                                arr[j++] = i.toNews();
                            }
                            Arrays.sort(arr, News.getComparator());
                            ArrayList<News> newses = new ArrayList<>(Arrays.asList(arr));
                            addCache(categoryId, newses);
                            return newses;
                        });
            } else {
                ret = Observable.just(newsCache.get(categoryId));
            }

            // Simulate some paging effect
            return ret.map(allItems -> {
                if (max_id == null) {
                    return new ArrayList<>(allItems.subList(0, Math.min(PAGE_SIZE, allItems.size())));
                }
                long max_id_ = Long.parseLong(max_id);
                int pos = 0;
                for (; pos < allItems.size() && allItems.get(pos).news_id >= max_id_; ++pos) ;
                return new ArrayList<>(allItems.subList(pos, Math.min(pos + PAGE_SIZE, allItems.size())));
            });
        }
    }

    private static API instance = null;

    public static API getInstance() {
        if (instance == null) {
            instance = new API();
        }
        return instance;
    }

}
