package com.ihandy.a2014011344;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by dc on 9/3/16.
 */
public class CachedAPI implements NewsAPI {

    private static final String TAG = "CachedAPI";
    public static final int MAX_CACHED_NEWS = 300; // 30 * 10

    NewsAPI api;
    Locale locale;
    DB dbWrapper;
    String catTableName, newsTableName, cmdClearCategories;

    CachedAPI(NewsAPI api_, Locale locale_, Context context) {
        api = api_;
        locale = locale_;
        dbWrapper = DB.i(context);
        catTableName = String.format(DB.CategoryEntry.DB_NAME, locale.toString());
        newsTableName = String.format(DB.NewsEntry.DB_NAME, locale.toString());
        cmdClearCategories = "DELETE FROM " + catTableName;
    }

    @Override
    public Observable<List<Category>> getCategories(String timestamp) {
        return api.getCategories(timestamp)
                .doOnNext((categories) -> {
                    dbWrapper.getWritableDB()
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.computation())
                            .subscribe((db) -> {
                                synchronized (db) {
                                    db.execSQL(cmdClearCategories);
                                    int id = 0;
                                    for (Category cat : categories) {
                                        ContentValues cv = new ContentValues();
                                        cv.put(DB.SQLGen.Table.COL_NAME_ID, ++id);
                                        cv.put(DB.CategoryEntry.COL_NAME_KEY, cat.id);
                                        cv.put(DB.CategoryEntry.COL_NAME_VALUE, cat.name);
                                        db.insert(catTableName, null, cv);
                                    }
                                }
                                Log.d(TAG, "getCategories: categories inserted");
                            }, e -> Log.e(TAG, "getCategories: ", e));
                });
    }

    @Override
    public Observable<List<News>> getNewsByCategory(String category, String max_id) {
        Func1<List<NewsAPI.News>, Action1<SQLiteDatabase>> writeToDB = (newsList) -> ((db) -> {
            String colId = DB.SQLGen.Table.COL_NAME_ID;
            Gson gson = new Gson();
            int nAdded = 0;
            synchronized (db) {
               /*
                long nEntries = DatabaseUtils.queryNumEntries(db, newsTableName);
                if (nEntries + newsList.size() > MAX_CACHED_NEWS) {
                    String cmd = String.format(
                            "DELETE FROM %s WHERE %s IN (SELECT %s FROM %s WHERE %s=%s)",
                            newsTableName, colId,
                            colId, newsTableName, DB.NewsEntry.COL_NAME_CATEGORY, category);
                    db.execSQL(cmd);
                } */
                for (News news : newsList) {
                    Cursor c = db.query(newsTableName, null, colId + "=" + news.news_id, null, null, null, null);
                    if (c == null || c.getCount() == 0) {
                        nAdded++;
                        ContentValues cv = new ContentValues();
                        cv.put(DB.SQLGen.Table.COL_NAME_ID, news.news_id);
                        cv.put(DB.NewsEntry.COL_NAME_CATEGORY, news.category);
                        cv.put(DB.NewsEntry.COL_NAME_SERIALIZED, gson.toJson(news));
                        db.replace(newsTableName, null, cv);
                    }
                    if (c != null) c.close();
                }
            }
            Log.d(TAG, "getNewsByCategory: " + nAdded + " news cached");
        });
        return api.getNewsByCategory(category, max_id)
                .doOnNext((newsList) -> dbWrapper.getWritableDB()
                        .observeOn(Schedulers.io())
                        .subscribeOn(Schedulers.computation())
                        .subscribe(writeToDB.call(newsList), e -> Log.e(TAG, "getNewsByCategory: ", e)));
    }
}
