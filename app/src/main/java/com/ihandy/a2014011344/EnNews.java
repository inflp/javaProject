package com.ihandy.a2014011344;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by dc on 8/27/16.
 */

public final class EnNews {

    interface APIWrapper { // wrapper for retrofit
        @GET("en/category")
        Observable<JsonObject> getCategories(@Query("timestamp") String timestamp);
        @GET("query")
        Observable<JsonObject> getNewsByCategory(
                @Query("locale") String locale,
                @Query("category") String category,
                @Query("max_news_id") String max_news_id);
        @GET("query")
        Observable<JsonObject> getLatestNewsByCategory(
                @Query("locale") String locale,
                @Query("category") String category);
    }


    public static class API implements NewsAPI {
        APIWrapper api_;

        API() {
            api_ = new Retrofit.Builder()
                    .baseUrl("http://assignment.crazz.cn/news/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build()
                    .create(APIWrapper.class);
        }

        @Override
        public Observable<List<NewsAPI.Category>> getCategories(String timestamp) {
            return api_.getCategories(timestamp).map(
                    (jsonObject) -> {
                        int code = jsonObject
                                .getAsJsonObject("meta")
                                .getAsJsonPrimitive("code").getAsInt();
                        if (code != 200) {
                            Log.e("EnNews", "getCategories: HTTP error " + jsonObject.toString());
                            throw new RuntimeException("HTTP error " + code); // Save time
                        }
                        JsonObject e = jsonObject.getAsJsonObject("data");
                        List<NewsAPI.Category> ret = new LinkedList<>();
                        for (Map.Entry<String, JsonElement> i : e.getAsJsonObject("categories").entrySet()) {
                            ret.add(new NewsAPI.Category(i.getKey(), i.getValue().getAsString()));
                        }
                        return ret;
                    });
        }

        @Override
        public Observable<List<NewsAPI.News>> getNewsByCategory(String category, String max_time) {
            Observable<JsonObject> init = max_time == null ?
                    api_.getLatestNewsByCategory("en", category) :
                    api_.getNewsByCategory("en", category, max_time);
            return init.map((jsonObject) -> {
                int code = jsonObject
                        .getAsJsonObject("meta")
                        .getAsJsonPrimitive("code").getAsInt();
                if (code != 200) {
                    Log.e("EnNews", "getNewsByCategory: HTTP error " + jsonObject.toString()
                            + " args: " + category + "; " + max_time);
                    throw new RuntimeException("HTTP error " + code); // Save time
                }
                JsonArray json = jsonObject
                        .getAsJsonObject("data")
                        .getAsJsonArray("news");
                NewsAPI.News[] newses = new Gson().fromJson(json, NewsAPI.News[].class);
                Arrays.sort(newses, News.getComparator());
                return Arrays.asList(newses);
            });
        }
    }

    private static API apiInstance = null;

    public static NewsAPI getInstance() {
        if (apiInstance == null) {
            apiInstance = new API();
        }
        return apiInstance;
    }
}
