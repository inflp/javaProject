package com.ihandy.a2014011344;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dc on 9/3/16.
 */
public class MainPresenter extends RxPresenter<MainActivity> {
    private static final String TAG = "MainPresenter";
    static MainPresenter instance = null;

    public static MainPresenter getInstance() { return instance; }

    public final int REQUEST_FROM_UI = 1;

    List<NewsAPI.Category> cachedCategories;

    @Override
    protected void onCreate(Bundle savedState) {
        instance = this;
        super.onCreate(savedState);
        restartableLatestCache(REQUEST_FROM_UI,
                () -> catListObservable(),
                (activity, response) -> updateActivityWithCats(activity, response, false),
                (activity, throwable) -> activity.onItemError(throwable)
        );

        cachedCategories = new ArrayList<>(0);
        Log.i(TAG, "onCreate: finished");
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy: invoked");
        instance = null;
        super.onDestroy();
    }

    public void setAppContext(Context appContext) {
        this.appContext = appContext;
    }

    Context appContext;

    Observable<List<NewsAPI.Category>> catListObservable() {
        return DataSource.getInstance(appContext).getAPI()
                .getCategories(Long.toString(System.currentTimeMillis()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    boolean onlineArrived = false;
    boolean dbJustDropped = false;
    int nUiItems = 0;

    // DON'T call onItems directly. Always use this method.
    void updateActivityWithCats(MainActivity activity, List<NewsAPI.Category> response, boolean isOffline) {
        if (response != null && !(isOffline && onlineArrived)) {
            // Update internals
            onlineArrived = onlineArrived || !isOffline;
            cachedCategories = response;

            // Update SettingsManager
            SettingsManager manager = SettingsManager.getInstance(appContext);
            NewsAPI.Locale locale = DataSource.getInstance(appContext).getLocale();
            manager.updateCategories(locale, response);

            // Send to UI. Remember to clone!
            List<NewsAPI.Category> respUI = new ArrayList<>();

            SettingsManager.SectionFilter filter = manager.getFilter(locale);
            for (NewsAPI.Category c: response) {
                for (String i: filter.catsOn) {
                    if (c.id.equals(i)) {
                        respUI.add(c);
                        break;
                    }
                }
            }

            // FAVORITE is injected here
            respUI.add(NewsPagePresenter.CAT_FAVORITE);
            nUiItems = respUI.size();

            activity.onItems(respUI);
        }
    }

    public void onDbDropped() {
        dbJustDropped = true;
    }

    public void onSettingsChanged(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        NewsAPI.Locale locale = NewsAPI.Locale.fromString(
                preferences.getString(SettingsActivity.PREF_NEWS_SRC, null));
        DataSource dataSource = DataSource.getInstance(appContext);
        if (dbJustDropped || locale != dataSource.getLocale()) {
            // Change source and start request.
            dataSource.changeSource(null, locale);
            request();
            dbJustDropped = false;
        } else {
            // Only need to handle enabling/disabling of categories.
            // Just push cachedCategories again; filtering is done in updateActivityWithCats.
            Observable.just(cachedCategories)
                    .compose(deliverFirst())
                    .subscribe(
                            (d) -> d.split((v, i) -> updateActivityWithCats(v, i, false), null),
                            (e) -> Log.e(TAG, "onSettingsChanged: ", e));
        }
    }

    public void onFragmentResumed(int position, NewsFragment fragment) {
        if (position + 1 == nUiItems) { // Favorites
            Log.d(TAG, "onFragmentResumed: updating favorites");
            fragment.getPresenter().requestFull();
        }
    }

    public void request() {
        start(REQUEST_FROM_UI);
    }

    public void initialRequest() {
        // Load cache from DB synchronously
        DataSource dataSource = DataSource.getInstance(appContext);
        dataSource.changeSource(DataSource.SourceType.OFFLINE, null);
        dataSource.getAPI()
                .getCategories(Long.toString(System.currentTimeMillis()))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(deliverFirst())
                .subscribe((delivery) -> {
                    delivery.split(
                            ((activity, cats) -> updateActivityWithCats(activity, cats, true)),
                            ((a, e) -> a.onItemError(e)));
                }, e -> Log.e(TAG, "initialRequest: ", e));
        // Send async request for online data
        dataSource.changeSource(DataSource.SourceType.ONLINE, null);
        start(REQUEST_FROM_UI);
    }
}
