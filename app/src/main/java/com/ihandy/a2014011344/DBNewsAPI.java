package com.ihandy.a2014011344;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

import rx.Observable;

/**
 * Created by dc on 9/3/16.
 */
public class DBNewsAPI implements NewsAPI {

    Locale locale;
    DB db;

    DBNewsAPI(Locale locale_, Context context) {
        locale = locale_;
        db = DB.i(context);
    }

    @Override
    public Observable<List<Category>> getCategories(String timestamp) {
        return db.queryDB(String.format(DB.CategoryEntry.DB_NAME, locale.toString()), null, null)
                .map(DB.cursorToObservable((cursor) -> {
                    final int id_key = cursor.getColumnIndex(DB.CategoryEntry.COL_NAME_KEY);
                    final int id_val = cursor.getColumnIndex(DB.CategoryEntry.COL_NAME_VALUE);
                    return new Category(cursor.getString(id_key), cursor.getString(id_val));
                }));
    }

    @Override
    public Observable<List<News>> getNewsByCategory(final String category, String max_id) {
        // always return all news
        return db.queryDB(
                String.format(DB.NewsEntry.DB_NAME, locale.toString()),
                String.format("%s = ?", DB.NewsEntry.COL_NAME_CATEGORY),
                new String[]{category})
                .map(DB.cursorToObservable((cursor -> {
                    final int id_serialized = cursor.getColumnIndex(DB.NewsEntry.COL_NAME_SERIALIZED);
                    String serialized = cursor.getString(id_serialized);
                    return new Gson().fromJson(serialized, News.class);
                })));
    }

}
