package com.ihandy.a2014011344;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class WebNewsActivity extends AppCompatActivity {
    public static final String ARG_NEWS_ACTIVITY_URI = "newsURI";
    public static final String ARG_NEWS_ACTIVITY_HASH = "newsHash";
    private static final String TAG = "WebNewsActivity";

    String mUri;
    int mHash;

    Toolbar toolbar;
    WebView webView, readerView;
    ProgressBar progressBar;
    boolean htmlProcessed;
    ViewPager viewPager;

    class WPagerAdapter extends PagerAdapter {
        View views[];
        boolean readerReady = false;

        WPagerAdapter(View views_[]) {
            views = views_;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return views[position];
        }

        @Override
        public int getCount() {
            return readerReady ? 2 : 1;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View)object;
        }
    }
    WPagerAdapter wPagerAdapter;

    void onReaderHtml(String html) {
        if (html.length() > 150) {
            Log.d(TAG, "onReaderHtml: " + html.length());
            readerView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);
            wPagerAdapter.readerReady = true;
            wPagerAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(1, true);
            Toast.makeText(this, "News re-typeset. Slide to return.", Toast.LENGTH_LONG).show();
        }
    }

    void startReader() {
        Observable.fromCallable(() -> {
            try {
                Log.i(TAG, "onCreate: Entering");
                String html_ = downloadHtml(mUri);
                String html = Readability.getInstance().convert(html_);
                html = getString(R.string.reader_html_h) + html + getString(R.string.reader_html_t);
                return html;
            } catch (Exception e) {
                Log.e(TAG, "onCreate: ", e);
                return null;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(html -> onReaderHtml(html),
                        e -> Log.e(TAG, "startReader: ", e));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_news);

        Intent intent = getIntent();
        mUri = intent.getStringExtra(ARG_NEWS_ACTIVITY_URI);
        mHash = intent.getIntExtra(ARG_NEWS_ACTIVITY_HASH, 0);
        htmlProcessed = false;

        // Set up toolbar as action bar
        toolbar = (Toolbar) findViewById(R.id.web_news_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.web_view_progress_bar);

        View views[] = {LayoutInflater.from(this).inflate(R.layout.webview_layout, null),
            LayoutInflater.from(this).inflate(R.layout.webview_layout, null)};
        webView = (WebView)views[0].findViewById(R.id.web_view_reader);
        readerView = (WebView)views[1].findViewById(R.id.web_view_reader);

        // region Set up webView
        webView.getSettings().setJavaScriptEnabled(true); // TODO make it an option

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setProgress(progress);
                }
            }

        });
        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "onReceivedError: " + errorCode + " " + description);
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                onReceivedError(view, error.getErrorCode(), error.getDescription().toString(), request.getUrl().toString());
            }
        });
        // endregion

        // region Set up simple pagerAdapter
        viewPager = (ViewPager)findViewById(R.id.pager);
        viewPager.addView(views[0]);
        viewPager.addView(views[1]);
        wPagerAdapter = new WPagerAdapter(views);
        viewPager.setAdapter(wPagerAdapter);
        // endregion

        if (PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean("pref_re_typeset", true)) {
            startReader();
        }
        webView.loadUrl(mUri);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    Boolean isFavoriteCache;
    NewsAPI.News mNewsCache;
    MenuItem favoriteMenuItem;
    ShareActionProvider shareActionProvider;

    boolean isFavorite() {
        if (isFavoriteCache == null) {
            Context c = getApplicationContext();
            mNewsCache = DataSource.getInstance(c).getNewsById(mHash);
            isFavoriteCache = FavoriteNewsHandler.getInstance(c).inFavorite(mNewsCache.news_id);
        }
        return isFavoriteCache;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Context c = getApplicationContext();
        FavoriteNewsHandler.getInstance(c).setFavorite(mNewsCache, isFavoriteCache);
        Log.d(TAG, "onPause: favorite status updating");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.web_news_menu, menu);

        favoriteMenuItem = menu.findItem(R.id.action_favorite);
        favoriteMenuItem.setIcon(isFavorite() ?
                        R.drawable.ic_action_favorite :
                        R.drawable.ic_action_favorite_border);

        shareActionProvider = (ShareActionProvider)
                MenuItemCompat.getActionProvider(menu.findItem(R.id.menu_item_share));
        if (shareActionProvider != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            String htmlText = String.format("%s - %s", mNewsCache.title, mUri);
            shareIntent.putExtra(Intent.EXTRA_TEXT, htmlText);
            shareActionProvider.setShareIntent(shareIntent);
            shareActionProvider.setOnShareTargetSelectedListener(
                (ShareActionProvider source, Intent intent) -> {
                    if (intent.getComponent().getPackageName().contains("tencent")) {
                        String className = intent.getComponent().getClassName();
                        NewsAPI.News news = DataSource.getInstance(getApplicationContext()).getNewsById(mHash);
                        if (className.contains("Favorite")) {
                            return false;
                        } else if (className.contains("ImgUI")) {
                            CustomShares.getInstance(getApplicationContext())
                                    .wxShare(news, true);
                            return false;
                        } else {
                            CustomShares.getInstance(getApplicationContext())
                                    .wxShare(news, false);
                            return false;
                        }
                    }
                    return false;
                });
        }

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onRefresh(MenuItem item) {
        progressBar.setVisibility(View.VISIBLE);
        webView.reload();
        return true;
    }

    public boolean onFavorite(MenuItem item) {
        isFavoriteCache = !isFavoriteCache;
        favoriteMenuItem.setIcon(isFavoriteCache ?
                R.drawable.ic_action_favorite :
                R.drawable.ic_action_favorite_border);
        return true;
    }

    public boolean onSendToWeChat(MenuItem item) {
        CustomShares.getInstance(getApplicationContext())
                .wxShare(DataSource.getInstance(getApplicationContext()).getNewsById(mHash), false);
        return true;
    }

     static String downloadHtml(String url) throws IOException {
        final OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }

        return response.body().string();
    }

}
