package com.ihandy.a2014011344;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.ihandy.a2014011344.NewsAPI.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import nucleus.factory.RequiresPresenter;

class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
    ArrayList<Category> categories;
    HashMap<Integer, Integer> positionById;
    HashMap<Integer, NewsFragment> fragmentByPosition;
    int idByPosition[];
    static int fIdCounter = 0;

    public ScreenSlidePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        Category cats[] = {NewsPagePresenter.CAT_FAVORITE};
        categories = new ArrayList<>(Arrays.asList(cats));
        positionById = new HashMap<>();
        idByPosition = new int[] {++fIdCounter};
        fragmentByPosition = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        // Called in creation
        int fId = idByPosition[position];
        Log.i("SSPager", String.format("getItem: pos=%d cat=%s id=%d",
                position, categories.get(position).id, fId));
        NewsFragment f = NewsFragment.newInstance(categories.get(position), fId);
        positionById.put(fId, position);
        fragmentByPosition.put(position, f);
        return f;
    }

    public NewsFragment getItemCached(int position) {
        return fragmentByPosition.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).name;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public int getItemPosition(Object object) {
        int id = ((NewsFragment)object).getFragmentId();
        Log.d("SSPager", "getItemPosition " + id);
        return positionById.get(id);
    }

    @Override
    public long getItemId(int position) {
        return idByPosition[position];
    }

    // Called on refresh or switching data source.
    void onItemsChanged(List<Category> categories_) {
        if (categories_.isEmpty()) {
            // TODO: 9/4/16  Distinguish 2 conditions when this happens
            // a. network error;
            // b. user just dropped database and network error
            return; // errors may have happened
        }

        if (categories_.equals(categories)) {
            // TODO: If the user previously clicked refresh, we need to refresh the current page
            // Where is appropriate for this?
            return;
        }

        ArrayList nCategories = new ArrayList<>(categories_);
        int nCats = nCategories.size(), nCats0 = categories.size();
        int newId[] = new int[nCats];
        for (int p = 0, q; p < nCats; ++p) {
            for (q = 0; q < nCats0; ++q) {
                if (categories.get(q).id.equals(nCategories.get(p))) {
                    break;
                }
            }
            if (q == nCats0) {
                // Allocate new fragment
                newId[p] = ++fIdCounter;
            }
            else {
                // Reuse & update & mark
                newId[p] = idByPosition[q];
                positionById.put(newId[p], p);
                idByPosition[q] = -1;
            }
        }

        // Invalidate all unsaved fragments
        for (int u = 0; u < nCats0; ++u) {
            if (idByPosition[u] != -1)
                positionById.put(idByPosition[u], POSITION_NONE);
        }

        // Update local storage
        idByPosition = newId;
        categories = nCategories;

        notifyDataSetChanged();
    }
}

@RequiresPresenter(MainPresenter.class)
public class MainActivity extends NucleusAppCompatActivity<MainPresenter>
        implements NewsFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";

    ViewPager viewPager;
    TabLayout tabLayout;
    ScreenSlidePagerAdapter pagerAdapter;
    Toolbar toolbar;
    DrawerLayout drawerLayout;

    /*
    static class PagerState {
        // guarantees: curPage always correct; prevPage valid if curPage is FAVORITE
        // Usually set through listener
        public static final int NONE = -1;
        int prevPage, curPage;
        PagerState() {
            prevPage = NONE;
            curPage = NONE;
        }
        void push(int c) {
            prevPage = curPage;
            curPage = c;
        }
    }
    PagerState pagerState; */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

/*        // Init local state
        pagerState = new PagerState();*/

        // Set up toolbar as action bar
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        // Register the hamburger button
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle =
                new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // Set up MainPager
        viewPager = (ViewPager) findViewById(R.id.main_pager);
        pagerAdapter = new ScreenSlidePagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        // Set up TabLayout
        tabLayout = (TabLayout) findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                toolbar.setTitle(pagerAdapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                // Inform presenter when page changed
                getPresenter().onFragmentResumed(position, pagerAdapter.getItemCached(position));
//                pagerState.push(position);
            }
        });

        // savedInstanceState is not null if the activity was destroyed due to resource constraints,
        // or a rotation of screen. In that case Nucleus presenter will push data to us.
        if (savedInstanceState == null) {
            getPresenter().setAppContext(getApplicationContext());
            getPresenter().initialRequest();
        } else {
            Log.i(TAG, "onCreate: creating from scratch");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(this.getLocalClassName(), "paused");
    }

    public void onItems(List<Category> categoryList) {
        pagerAdapter.onItemsChanged(categoryList);
    }

    public void onItemError(Throwable e) {
        Snackbar.make(viewPager, "Cannot fetch news: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        Log.e(TAG, "onItemError: ", e);
    }

    // region Transitional events

    boolean navToSettings = false, navToWebNews = false;

    public void onSettingsClicked(MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START, true);
        Intent intent = new Intent(this, SettingsActivity.class);
        navToSettings = true;
        startActivity(intent);
    }

    public void onFavoriteClicked(MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START, true);
        viewPager.setCurrentItem(tabLayout.getTabCount() - 1);
    }

    public void onAboutClicked(MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START, true);
        TextView textView = new TextView(this);
        textView.setText(Html.fromHtml(this.getString(R.string.html_about)));
        textView.setPadding(30, 30, 0, 0);
        new AlertDialog.Builder(this)
                .setView(textView)
                .setPositiveButton("OK", (d, i) -> {})
                .create().show();
    }

/*
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pagerState.curPage == pagerAdapter.getCount() - 1 // on favorite page
                    && pagerState.prevPage != PagerState.NONE) { // e.g. no cats loaded after startup
                viewPager.setCurrentItem(pagerState.prevPage);
                return true; // Stop propagating event
            }
        }
        return super.onKeyUp(keyCode, event);
    }
*/

    @Override
    protected void onResume() {
        super.onResume();
        if (navToSettings) {
            // We just returned from SettingsActivity. Update configs
            navToSettings = false;
            getPresenter().onSettingsChanged(this);
        } else if (navToWebNews) {
            // Tabs and fragments are unchanged.
            // This is useful when we removed something in WebNewsActivity from favorite page.
            navToWebNews = false;
            int pos = tabLayout.getSelectedTabPosition();
            getPresenter().onFragmentResumed(pos, pagerAdapter.getItemCached(pos));
        }
    }

    public boolean onRefresh(MenuItem m) {
        // Do ``global refresh''
        getPresenter().request();
        return true;
    }

    // Called from fragments
    @Override
    public void onNewsItemSelected(NewsAPI.News news) {
        navToWebNews = true;
//        Intent intent = new Intent(this, WebNewsActivity.class);
        Intent intent = new Intent(this, WebNewsActivity.class);
        intent.putExtra(WebNewsActivity.ARG_NEWS_ACTIVITY_HASH, DataSource.getInstance(this).getNewsID(news));
        intent.putExtra(WebNewsActivity.ARG_NEWS_ACTIVITY_URI, (news.source == null ? null : news.source.url));
        startActivity(intent);
    }

    // endregion
}
