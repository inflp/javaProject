package com.ihandy.a2014011344;

import android.content.Context;
import android.support.v7.preference.PreferenceManager;

import com.ihandy.a2014011344.NewsAPI.News;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;

/**
 * Created by dc on 8/30/16.
 */
public class DataSource {
    private static final String TAG = "DataSource";

    enum SourceType {
        ONLINE, OFFLINE
    }
    SourceType sourceType;
    NewsAPI.Locale locale;
    HashMap<Integer, News> newsHashMap;

    int getNewsID(News news) {
        if (newsHashMap == null) {
            newsHashMap = new HashMap<>();
        }
        int hash = news.hashCode();
        if (! newsHashMap.containsKey(hash)) {
            newsHashMap.put(hash, news);
        }
        return hash;
    }

    News getNewsById(int hash) {
        return newsHashMap.get(hash);
    }

    static class NullAPI implements NewsAPI {
        @Override
        public Observable<List<Category>> getCategories(String timestamp) {
            return Observable.just(new ArrayList<>());
        }
        @Override
        public Observable<List<News>> getNewsByCategory(String category, String max_id) {
            return Observable.just(new ArrayList<>());
        }
    }

    NewsAPI enOnline, chOnline;
    NewsAPI enOffline, chOffline;

    NewsAPI getAPI(NewsAPI.Locale locale, SourceType sourceType) {
        switch (sourceType) {
            case ONLINE:
                return locale == NewsAPI.Locale.EN ? enOnline : chOnline;
            case OFFLINE:
                return locale == NewsAPI.Locale.EN ? enOffline : chOffline;
        }
        throw new RuntimeException();
    }
    NewsAPI getAPI(SourceType sourceType) {
        return getAPI(locale, sourceType);
    }
    NewsAPI getAPI(NewsAPI.Locale locale) {
        return getAPI(locale, sourceType);
    }
    NewsAPI getAPI() {
        return getAPI(sourceType);
    }

    DataSource(Context context) {

        // I don't know the default behavior of PM, so don't use fromString.
        String localeStr = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(SettingsActivity.PREF_NEWS_SRC, NewsAPI.Locale.EN.toString());
        if (localeStr.equals(NewsAPI.Locale.CN.toString())) {
            locale = NewsAPI.Locale.CN;
        } else {
            locale = NewsAPI.Locale.EN;
        }

        sourceType = SourceType.ONLINE;
        enOnline = new CachedAPI(EnNews.getInstance(), NewsAPI.Locale.EN, context);
        chOnline = new CachedAPI(CnNews.getInstance(), NewsAPI.Locale.CN, context);
        enOffline = new DBNewsAPI(NewsAPI.Locale.EN, context);
        chOffline = new DBNewsAPI(NewsAPI.Locale.CN, context);
    }

    public void changeSource(SourceType n_sourceType, NewsAPI.Locale n_locale) {
        if (n_sourceType != null) sourceType = n_sourceType;
        if (n_locale != null) locale = n_locale;
    }
    public NewsAPI.Locale getLocale() {
        return locale;
    }

    private static DataSource instance;

    public static DataSource getInstance(Context context) {
        if (instance == null) {
            instance = new DataSource(context);
        }
        return instance;
    }
}

