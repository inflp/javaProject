package com.ihandy.a2014011344;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

/**
 * Created by dc on 9/6/16.
 */
public class CustomShares {
    private static final String WECHAT_ID = "wx39fbb598aa8c43a5";
    private IWXAPI iwxapi;

    Context context;

    CustomShares(Context c) {
        context = c;
        iwxapi = WXAPIFactory.createWXAPI(c, WECHAT_ID, true);
        iwxapi.registerApp(WECHAT_ID);
    }

    public static CustomShares getInstance(Context c) {
        if (instance == null) {
            instance = new CustomShares(c);
        }
        return instance;
    }

    private static CustomShares instance;

    public void wxShare(NewsAPI.News news, boolean toSession) {
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = news.source.url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = news.title;
        msg.description = ""; // TODO
        msg.setThumbImage(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = toSession ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;
        iwxapi.sendReq(req);
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

}
