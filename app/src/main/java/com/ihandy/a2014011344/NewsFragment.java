package com.ihandy.a2014011344;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import nucleus.factory.RequiresPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RequiresPresenter(NewsPagePresenter.class)
public class NewsFragment extends NucleusSupportFragment<NewsPagePresenter> {
    private static final String TAG = "NewsFragment";
    private static final String ARG_CAT_NAME = "arg_cat_name";
    private static final String ARG_CAT_ID = "arg_cat_id";
    private static final String ARG_FRAG_ID = "arg_frag_id";
    public static final int ID_FAVORITE = -233;

    String mCategoryName, mCategoryId;
    private int mFragmentId;

    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    MainRecyclerAdapter adapter;
    SwipyRefreshLayout swipeRefresher;

    private void stopRefresher() {
        if (swipeRefresher.isRefreshing()) {
            swipeRefresher.setRefreshing(false);
        }
    }

    public int getFragmentId() {
        return mFragmentId;
    }

    public void onItems(List<NewsAPI.News> news, int headPos) {
        Log.d(getTag(), this.hashCode() + " " +
                news.size() + "items arrived (cat " + mCategoryId + ") obj" + news.hashCode());
        adapter.onLatestItems(new ArrayList<>(news));
        // Try adjusting item height if asked
        if (headPos >= 0 && recyclerView.getChildCount() > 0) {
            layoutManager.scrollToPositionWithOffset(headPos, 0);
        }
        stopRefresher();
    }
    public void onItemError(Throwable e) {
        Snackbar.make(getView(), "Cannot fetch news: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        Log.e(getTag(), "onItemError: " + e.toString());
        stopRefresher();
    }

    public NewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param category Parameter 1.
     * @return A new instance of fragment NewsFragment.
     */
    public static NewsFragment newInstance(NewsAPI.Category category, int fragmentId) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CAT_NAME, category.name);
        args.putString(ARG_CAT_ID, category.id);
        args.putInt(ARG_FRAG_ID, fragmentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryName = getArguments().getString(ARG_CAT_NAME);
            mCategoryId = getArguments().getString(ARG_CAT_ID);
            mFragmentId = getArguments().getInt(ARG_FRAG_ID);
        } else {
            Log.e(getTag(), "getArguments() returns null");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_page, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated called (" + mCategoryId + ", " + mCategoryName + ")");

        // Set up swipe refresh animation
        swipeRefresher = (SwipyRefreshLayout) getView().findViewById(R.id.main_swipe_refresh);
        if (! mCategoryId.equals(NewsPagePresenter.CAT_FAVORITE.id)) {
            swipeRefresher.setOnRefreshListener((direction) -> {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    getPresenter().requestLatest();
                } else {
                    getPresenter().requestPrevious();
                }
            });
        } else {
            swipeRefresher.setOnRefreshListener((direction) -> {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    getPresenter().requestFull();
                } else {
                    stopRefresher();
                }
            });
        }

        // Init recycler view
        recyclerView = (RecyclerView) getView().findViewById(R.id.main_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MainRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));

        // Add news click event
        adapter.getPositionClicks().observeOn(AndroidSchedulers.mainThread())
                .subscribe((news) -> {
                    if (mListener != null) {
                        mListener.onNewsItemSelected(news);
                    }
                }, e -> Log.e(TAG, "onActivityCreated: ", e));

        if (savedInstanceState == null) {
            getPresenter().onViewReady(getActivity().getApplicationContext(), mCategoryId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        // TODO: Should we nullify UI elems?
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onNewsItemSelected(NewsAPI.News news);
    }
}


class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTop, textBot;
        public ImageView rvIcon;
        public ViewHolder (View view) {
            super(view);
            textTop = (TextView) view.findViewById(R.id.textTop);
            textBot = (TextView) view.findViewById(R.id.textBot);
            rvIcon = (ImageView) view.findViewById(R.id.rvIcon);
        }
    }

    ArrayList<NewsAPI.News> mDataSet;
    private final PublishSubject<NewsAPI.News> onClickSubject = PublishSubject.create();

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_rvitem, parent, false);
        return new ViewHolder(v);
    }

    // Replace the content of the position-th ViewHolder holder (invoked by layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NewsAPI.News news = mDataSet.get(position);

        // Footer
        String disp_id = Long.toString(news.news_id);
        disp_id = disp_id.substring(0, Math.min(6, disp_id.length() - 1));
        if (news.source == null) {
            disp_id += " - no content";
        }
        String footer = String.format("<font color=\"#006621\">%s</font> - <font color=\"#bbbbbb\">id %s</font>",
                news.origin, disp_id);
        holder.textBot.setText(Html.fromHtml(footer));

        // Header
        String header = news.title;
//        String header = String.format("<font color=\"#000000\">%s</font>", news.title);
        holder.textTop.setText(Html.fromHtml(header));

        // Image
        Context context = holder.rvIcon.getContext();
        if (news.imgs == null || news.imgs.isEmpty() || news.imgs.get(0) == null ||
                news.imgs.get(0).url == null || news.imgs.get(0).url.length() < 2) {
            holder.rvIcon.setImageDrawable(
                    ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
        } else {
            Picasso.with(context)
                    .load(news.imgs.get(0).url)
                    .placeholder(R.mipmap.ic_launcher)
                    .resize(150, 150)
                    .centerCrop()
                    .error(R.mipmap.ic_launcher)
                    .into(holder.rvIcon);
        }

        // Clickable
        if (news.source != null) {
            holder.itemView.setOnClickListener((v) -> onClickSubject.onNext(news));
        } else {
            holder.itemView.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public MainRecyclerAdapter() {
        mDataSet = new ArrayList<>();
    }

    private static final String TAG = "MainRecyclerAdapter";

    public void onLatestItems(ArrayList<NewsAPI.News> items) {
        mDataSet.clear();
        mDataSet.addAll(items);
        notifyDataSetChanged();
    }

    public Observable<NewsAPI.News> getPositionClicks() {
        return onClickSubject.asObservable();
    }
}

