package com.ihandy.a2014011344;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.ihandy.a2014011344.NewsAPI.Category;
import com.ihandy.a2014011344.NewsAPI.News;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dc on 9/4/16.
 * Notice it seems to create a new presenter for each fragment
 */
public class NewsPagePresenter extends RxPresenter<NewsFragment> {
    private static final String TAG = "NewsPagePresenter";
    static final int REQUEST_LATEST = 2;
    static final int REQUEST_PREVIOUS = 3;
    static final int REQUEST_FULL = 4;
    public static final String ARG_CAT_ID = "categoryID";
    public static final Category CAT_FAVORITE = new Category("__FAVORITE__", "Favorites");

    private String categoryID;
    private String maxNewsID;
    boolean initialItemsDelivered;
    private Context appContext;

    private boolean isFavorite() {
        return categoryID.equals(CAT_FAVORITE.id);
    }

    private DataSource getDataSource() {
        return DataSource.getInstance(appContext);
    }

    List<News> curNewsList;
    Long minNewsId;

    private void updateWithLatest(NewsFragment view, List<News> latest) {
        TreeSet<News> newsSet = new TreeSet<>(News.getComparator());
        for (News n: latest) {
            newsSet.add(n);
        }
        for (News n: curNewsList) {
            newsSet.add(n);
        }
        curNewsList.clear();
        for (News n: newsSet) {
            curNewsList.add(n);
            if (minNewsId == null || minNewsId > n.news_id)
                minNewsId = n.news_id;
        }
        view.onItems(curNewsList, -1);
    }
    private void updateWithPrev(NewsFragment view, List<News> prev) {
        int prevPos = curNewsList.size() - 1;
        for (News n: prev) {
            if (! curNewsList.contains(n)) {
                curNewsList.add(n);

                if (minNewsId == null || minNewsId > n.news_id)
                    minNewsId = n.news_id;
            }
        }
        view.onItems(curNewsList, Math.max(prevPos, 0));
    }
    private void fullUpdate(NewsFragment view, List<News> full) {
        view.onItems(full, -1);
    }

    Observable<List<News>> getNewsObservable() {
        if (isFavorite()) {
            return FavoriteNewsHandler.getInstance(appContext).getFavoriteList();
        }
        return getDataSource().getAPI()
                        .getNewsByCategory(categoryID, maxNewsID)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        if (savedState != null) {
            categoryID = savedState.getString(ARG_CAT_ID);
        }
        Log.i(TAG, "onCreate: called " + this);
        initialItemsDelivered = false;
        curNewsList = new ArrayList<>();
        restartableLatestCache(REQUEST_LATEST,
                () -> getNewsObservable(),
                (view, list) -> updateWithLatest(view, list),
                (view, e) -> view.onItemError(e));
        restartableLatestCache(REQUEST_PREVIOUS,
                () -> getNewsObservable(),
                (v, l) -> updateWithPrev(v, l),
                (v, e) -> v.onItemError(e));
        restartableLatestCache(REQUEST_FULL,
                () -> getNewsObservable(),
                (v, l) -> fullUpdate(v, l),
                (v, e) -> v.onItemError(e));
    }

    @Override
    protected void onSave(Bundle state) {
        super.onSave(state);
        state.putString(ARG_CAT_ID, categoryID);
    }

    void requestFull() {
        if (initialItemsDelivered) {
            maxNewsID = null;
            start(REQUEST_FULL);
        }
    }

    void requestLatest() {
        if (initialItemsDelivered) {
            maxNewsID = null;
            start(REQUEST_LATEST);
        }
    }

    void requestPrevious() {
        if (!isFavorite() && initialItemsDelivered) {
            maxNewsID = minNewsId == null ? null : minNewsId.toString();
            start(REQUEST_PREVIOUS);
        }
    }

    void onViewReady(Context appContext, String categoryID_) {
        Log.i(TAG, "onViewReady: " + this.hashCode() + " " + categoryID_);
        this.appContext = appContext;
        categoryID = categoryID_;
        // Request initial items (offline, effectively blocking)
        initialItemsDelivered = false;
        Observable<List<News>> initItems;
        if (isFavorite()) {
            initItems = FavoriteNewsHandler.getInstance(appContext).getFavoriteList();
        } else {
            initItems = getDataSource()
                    .getAPI(DataSource.SourceType.OFFLINE)
                    .getNewsByCategory(categoryID_, null);
        }
        initItems.compose(deliverFirst())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((delivery) -> {
                    initialItemsDelivered = true;
                    if (!isFavorite()) {
                        delivery.split((v, l) -> updateWithLatest(v, l),
                                (v, e) -> v.onItemError(e));
                        // Start async request for online data
                        requestLatest();
                    } else {
                        delivery.split((v, l) -> fullUpdate(v, l),
                                (v, e) -> v.onItemError(e));
                    }
                }, e -> Log.e(TAG, "onViewReady: ", e));
    }
}
