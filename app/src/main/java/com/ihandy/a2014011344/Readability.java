package com.ihandy.a2014011344;

import android.util.Log;

import com.wuman.Reader;

import java.util.List;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.EcmaError;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.ScriptableObject;

public class Readability {

    Context context;
    ScriptableObject scope;
    Reader reader;
    Function f;

    static Readability instance;

    public synchronized static Readability getInstance() {
        if (instance == null) {
            instance = new Readability();
        }
        return instance;
    }

    private Readability() {
    }

    public String convert(String input) {
        context = ContextFactory.getGlobal().enterContext();
        scope = context.initStandardObjects();
        context.setOptimizationLevel(-1);
        reader = new Reader();
        reader.exec(context, scope);
        f = (Function) scope.get("convertHtml", scope);
        try {
            Object r = f.call(context, scope, scope, new Object[]{input});
            return (String)r;
        } catch (Exception e) {
            // Parsing / converting error, usually
            Log.e(TAG, "convert: ", e);
            return null;
        }
    }

    private static final String TAG = "Readability";
/*
    public static void main(String[] args) throws Exception {
        List<String> lines = Files.readAllLines(Paths.get("/tmp/a.html"));
        String inp = "";
        for (String s: lines) inp += s + "\n";
        long t = System.currentTimeMillis(), u = 0;
        Readability r = new Readability();
        for (int i = 0; i < 10; ++i) {
            u += r.convert(inp).length();
        }
        System.out.println((System.currentTimeMillis() - t));
    } */
}
