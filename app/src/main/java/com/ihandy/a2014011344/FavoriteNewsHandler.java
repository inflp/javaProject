package com.ihandy.a2014011344;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by dc on 9/4/16.
 */
public class FavoriteNewsHandler {

    DB db;

    FavoriteNewsHandler(Context c) {
        db = DB.i(c);
    }

    public static FavoriteNewsHandler getInstance(Context c) {
        if (instance == null) {
            instance = new FavoriteNewsHandler(c);
        }
        return instance;
    }

    static FavoriteNewsHandler instance;

    boolean inFavorite(long newsId) {
        return db.queryDB(DB.FavoriteEntry.DB_NAME,
                DB.SQLGen.Table.COL_NAME_ID + "=" + newsId, null)
                .map(cursor -> {
                    boolean ret = cursor != null && cursor.getCount() > 0;
                    if (cursor != null) {
                        cursor.close();
                    }
                    return ret;
                }).toBlocking().first();
    }
    void setFavorite(NewsAPI.News news, boolean value) {
        db.getWritableDB().doOnNext(db -> {
            long newsId = news.news_id;
            if (value) {
                if (! inFavorite(newsId)) {
                    ContentValues cv = new ContentValues();
                    cv.put(DB.SQLGen.Table.COL_NAME_ID, newsId);
                    cv.put(DB.FavoriteEntry.COL_NAME_SERIALIZED, (new Gson()).toJson(news));
                    db.insert(DB.FavoriteEntry.DB_NAME, null, cv);
                }
            } else if (inFavorite(newsId)) {
                db.delete(DB.FavoriteEntry.DB_NAME, DB.SQLGen.Table.COL_NAME_ID + "=" + newsId, null);
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
                .subscribe(d -> {}, e -> Log.e(TAG, "setFavorite: ", e));
    }
    Observable<List<NewsAPI.News>> getFavoriteList() {
        final Gson gson = new Gson();
        return db.queryDB(DB.FavoriteEntry.DB_NAME, null, null)
                .map(DB.cursorToObservable(cursor -> {
                    int i = cursor.getColumnIndex(DB.FavoriteEntry.COL_NAME_SERIALIZED);
                    return gson.fromJson(cursor.getString(i), NewsAPI.News.class);
                }));
    }

    private static final String TAG = "FavoriteNewsHandler";
}
