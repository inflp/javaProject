package com.ihandy.a2014011344;

import android.support.annotation.Nullable;

import java.util.Comparator;
import java.util.List;

import rx.Observable;

/**
 * Created by dc on 8/30/16.
 */

public interface NewsAPI {
    enum Locale {
        EN, CN;
        @Override
        public String toString() {
            return this.equals(EN) ? "en" : "cn";
        }
        public static Locale fromString(String s) {
            return s.equals(EN.toString()) ? EN : CN;
        }
    }

    public class News {
        public static class Url {
            public String url;
        }
        public String category, country;
        @Deprecated
        public String fetched_time;
        public List<Url> imgs;
        public String locale_category;
        public long news_id;
        public String origin, relative_news;
        public static class Source {
            public String name, url;
        }
        @Nullable
        public Source source;
        public String title;
        @Deprecated
        public String updated_time;
        static Comparator<News> getComparator() {
            return (a, b) -> a.news_id == b.news_id ? 0 :
                    a.news_id > b.news_id ? -1 : 1;
        }

        @Override
        public int hashCode() {
            return Long.valueOf(news_id).hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return (obj instanceof News) && news_id == ((News)obj).news_id;
        }
    }

    class Category {
        String id, name;
        public Category(String i, String n) {
            id = i;
            name = n;
        }

        @Override
        public String toString() {
            return String.format("%s: %s", id, name);
        }

        @Override
        public int hashCode() {
            return id.concat("$$").concat(name).hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (! (obj instanceof Category)) return false;
            Category nc = (Category)obj;
            return nc.id.equals(id) && nc.name.equals(name);
        }
    }

    Observable<List<Category>> getCategories(String timestamp);
    Observable<List<News>> getNewsByCategory(String category, String max_id);
}
