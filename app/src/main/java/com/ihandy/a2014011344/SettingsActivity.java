package com.ihandy.a2014011344;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v14.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import java.util.*;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msetting);

        // Set up action bar
        setSupportActionBar((Toolbar)findViewById(R.id.setting_toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.settings);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public static class GeneralPreferenceFragment extends PreferenceFragment {

        private static final String TAG = "GPFragment";

        private static void addListener(
                Preference p, Preference.OnPreferenceChangeListener preferenceListener) {
            // Set the listener to watch for value changes.
            p.setOnPreferenceChangeListener(preferenceListener);

            SharedPreferences dsp = PreferenceManager
                    .getDefaultSharedPreferences(p.getContext());

            // Trigger the listener immediately with the preference's
            // current value.
            Object value;
            if (p instanceof SwitchPreference) {
                value = dsp.getBoolean(p.getKey(), false);
            } else if (! (p instanceof MultiSelectListPreference)) {
                value = dsp.getString(p.getKey(), "");
            } else {
                value = dsp.getStringSet(p.getKey(), null);
            }
            preferenceListener.onPreferenceChange(p, value);
        }

        void updateCategoryPreferences(String catPrefId, SettingsManager.SectionFilter filter) {
            MultiSelectListPreference pref =
                    (MultiSelectListPreference) findPreference(catPrefId);

            // Update preference object
            int nCats = filter.catNames.size();
            String catNames[] = new String[nCats];
            String catValues[] = new String[nCats];
            for (int i = 0; i < nCats; ++i) {
                catNames[i] = filter.catNames.get(i);
                catValues[i] = filter.catIds.get(i);
            }
            pref.setEntries(catNames);
            pref.setEntryValues(catValues);
            Set<String> selected = new TreeSet<>();
            selected.addAll(filter.catsOn);
            pref.setValues(selected);
            pref.setSummary("Showing " + selected.size() + " sections");
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.pref_general);
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // TODO: Test first run
            ListPreference pref_locale = (ListPreference) findPreference(PREF_NEWS_SRC);
            pref_locale.setEntries(new String[]{"English", "Chinese"});
            pref_locale.setEntryValues(new String[]{NewsAPI.Locale.EN.toString(), NewsAPI.Locale.CN.toString()});
            if (pref_locale.getValue() == null) {
                pref_locale.setValue(NewsAPI.Locale.EN.toString());
            }
        }

        SettingsManager settingsManager;

        public void onActivityCreated(@Nullable Bundle bundle) {
            super.onActivityCreated(bundle);

            Context context = getActivity().getApplicationContext();
            settingsManager = SettingsManager.getInstance(context);

            // region Manage sections
            String[] pIDs = {PREF_SECTIONS_CN, PREF_SECTIONS_EN};
            for (String pID: pIDs) {
                MultiSelectListPreference pref = (MultiSelectListPreference)findPreference(pID);
                pref.setSummary("Retrieving categories");
                pref.setEnabled(false);

                final NewsAPI.Locale locale =
                        pID.equals(PREF_SECTIONS_CN) ? NewsAPI.Locale.CN : NewsAPI.Locale.EN;
                settingsManager.syncCategoriesOnNull(locale)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(nil -> {
                            SettingsManager.SectionFilter res = settingsManager.getFilter(locale);
                            pref.setEnabled(true);
                            updateCategoryPreferences(pID, res);
                            addListener(findPreference(pID), (me, value) -> {
                                Set<String> v_set = (Set<String>)value;
                                int nItems = v_set == null ? 0 : v_set.size();
                                me.setSummary("Showing " + nItems + " sections");
                                return true;
                            });
                        }, e -> Log.e(TAG, "onActivityCreated: ", e));
            }
            // endregion

            // region Manage news source
            addListener(findPreference(PREF_NEWS_SRC), (me, value) -> {
                ListPreference listPreference = (ListPreference) me;
                String s_val = (String)value;

                // Update summary
                int index = listPreference.findIndexOfValue(s_val);
                me.setSummary(index >= 0
                        ? listPreference.getEntries()[index]
                        : null);

                return true;
            });
            // endregion

            // region Dev options
            Preference prefDropDb = findPreference(PREF_DROP_DB);
            if (prefDropDb != null) {
                prefDropDb.setOnPreferenceClickListener((me) -> {
                    new AlertDialog.Builder(getActivity())
                            .setMessage("Remove the database?")
                            .setNegativeButton("Cancel", (d, i) -> {
                            })
                            .setPositiveButton("OK", (d, i) -> {
                                DB.i(context).dropDB()
                                        .observeOn(Schedulers.io())
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(n -> MainPresenter.getInstance().onDbDropped(),
                                                e -> Log.e(TAG, "onActivityCreated: ", e));
                            })
                            .create()
                            .show();
                    return true;
                });
            }

            findPreference(PREF_ABOUT).setOnPreferenceClickListener(me -> {
                TextView textView = new TextView(getActivity());
                textView.setText(Html.fromHtml(getActivity().getString(R.string.html_about)));
                textView.setPadding(30, 30, 0, 0);
                new AlertDialog.Builder(getActivity())
                        .setView(textView)
                        .setPositiveButton("OK", (d, i) -> {})
                        .create().show();
                return true;
            });
            // endregion
        }

        @Override
        public void onPause() {
            if (settingsManager != null) {
                settingsManager.updateFilters(
                        ((MultiSelectListPreference)findPreference(PREF_SECTIONS_EN)).getValues(),
                        ((MultiSelectListPreference)findPreference(PREF_SECTIONS_CN)).getValues());
            }
            super.onPause();
        }
    }

    public static final String PREF_NEWS_SRC = "pref_news_src";
    public static final String PREF_SECTIONS_CN = "pref_sections_cn";
    public static final String PREF_SECTIONS_EN = "pref_sections_en";
    public static final String PREF_DROP_DB = "pref_drop_db";
    public static final String PREF_ABOUT = "pref_about";

}
