package com.ihandy.a2014011344;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by dc on 8/30/16.
 */

public class DB {

    private static final String TAG = "DB";

    NewsDBHelper dbHelper;
    SQLiteDatabase readableDB, writableDB;

    // region Singleton declaration
    static DB instance = null;

    static DB i(Context c) {
        if (instance == null) {
            instance = new DB(c);
        }
        return instance;
    }
    // endregion

    public DB(Context context) {
        dbHelper = new NewsDBHelper(context);
    }

    static <T> Func1<Cursor, List<T>> cursorToObservable(Func1<Cursor, T> mapper) {
        return (cursor) -> {
            ArrayList<T> ret = new ArrayList<>();
            if (cursor != null) { // Table may be empty
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    ret.add(mapper.call(cursor));
                }
                cursor.close();
            }
            return ret;
        };
    }

    public Observable<SQLiteDatabase> getWritableDB() {
        return Observable.fromCallable(() -> {
            checkInitialized();
            if (writableDB != null) {
                return writableDB;
            }
            writableDB = dbHelper.getWritableDatabase();
            return writableDB;
        });
    }

    // Only exposed for tests
    synchronized boolean checkInitialized() {
        Log.d(TAG, "checkInitialized: entering method");
        if (readableDB == null) {
            readableDB = dbHelper.getReadableDatabase();
            Cursor cursor = readableDB.rawQuery(
                    "select DISTINCT tbl_name from sqlite_master where tbl_name = '" + ConfigEntry.DB_NAME + "'",
                    null);
            boolean isEmpty = cursor == null || cursor.getCount() == 0;
            if (cursor != null) {
                cursor.close();
            }
            if (isEmpty) {
                Log.d(TAG, "checkInitialized: creating tables");
                dbHelper.onCreate(dbHelper.getWritableDatabase());
            }
            return !isEmpty;
        }
        // Since this is the only method to set readableDB, and dropDB nullify readableDB
        return true;
    }

    interface DBDropListener {
        void onDbDropped();
    }

    HashSet<DBDropListener> listeners = new HashSet<>();

    // TODO: Let MainPresenter use this. Need to test presenter's lifecycle first.
    void addDbDropListener(DBDropListener l) {
        listeners.add(l);
    }

    // This is supposed to use in tests only.
    public Observable<Void> dropDB() {
        return getWritableDB().map((db) -> {
            dbHelper.dropTables(db);
            readableDB.close();
            writableDB.close();
            readableDB = null;
            writableDB = null;
            Log.i(TAG, "dropDB: finished");

            for (DBDropListener l: listeners) {
                l.onDbDropped();
            }

            return null;
        });
    }

    Observable<Cursor> queryDB(String tableName, String where, String[] whereArgs) {
        return Observable.fromCallable(() -> {
            checkInitialized();
            return readableDB.query(tableName, null, where, whereArgs, null, null, null);
        });
    }

    // DB-related things.
    // A painful experience without macro ...
    static class SQLGen {
        static class Table {
            public static final String COL_NAME_ID = "ID";
            String[] columns;
            public final String createDB, deleteDB, tableName;
            Table(String[] c, String tableName) {
                this.columns = c;
                this.tableName = tableName;
                String createDB_;
                createDB_ = "CREATE TABLE " + tableName + " (ID INTEGER PRIMARY KEY, ";
                for (int i = 0; i < columns.length; ++i) {
                    createDB_ += columns[i] + " TEXT";
                    if (i + 1 == columns.length) {
                        createDB_ += ")";
                    } else {
                        createDB_ += ", ";
                    }
                }
                createDB = createDB_;
                deleteDB = "DROP TABLE IF EXISTS " + tableName;
            }
            static <T> Table create(Class<T> c, String args[]) {
                Field fs[] = c.getDeclaredFields();
                ArrayList<String> cols = new ArrayList<>();
                try {
                    for (Field f: fs) {
                        if (f.getType().equals(String.class) && Modifier.isStatic(f.getModifiers()) &&
                                f.getName().startsWith("COL_NAME_")) {
                            String fmt = (String) f.get(null);
                            cols.add(String.format(fmt, (Object[])args));
                        }
                    }
                    String tableName = (String) c.getField("DB_NAME").get(null);
                    tableName = String.format(tableName, (Object[])args);
                    return new Table(cols.toArray(new String[cols.size()]), tableName);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e.toString());
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e.toString());
                }
            }
        }
    }

    static class NewsEntry {
        public static final String COL_NAME_CATEGORY = "category";
        public static final String COL_NAME_SERIALIZED = "serialized";
        public static final String DB_NAME = "news_%s";
    }

    static class CategoryEntry {
        public static final String COL_NAME_KEY = "key";
        public static final String COL_NAME_VALUE = "value";
        public static final String DB_NAME = "categories_%s";
    }

    static class ConfigEntry {
        public static final String COL_NAME_KEY = "key";
        public static final String COL_NAME_VALUE = "value";
        public static final String DB_NAME = "config";
    }

    static class FavoriteEntry {
        public static final String COL_NAME_SERIALIZED = "serialized";
        public static final String DB_NAME = "favorites";
    }

    static class SQLCollection {
        public final ArrayList<String> createCmds, deleteCmds;
        SQLCollection() {
            SQLGen.Table tables[] = {
                    SQLGen.Table.create(NewsEntry.class, new String[] {"cn"}),
                    SQLGen.Table.create(NewsEntry.class, new String[] {"en"}),
                    SQLGen.Table.create(CategoryEntry.class, new String[] {"cn"}),
                    SQLGen.Table.create(CategoryEntry.class, new String[] {"en"}),
                    SQLGen.Table.create(ConfigEntry.class, null),
                    SQLGen.Table.create(FavoriteEntry.class, null)};
            createCmds = new ArrayList<>();
            deleteCmds = new ArrayList<>();
            for (SQLGen.Table t: tables) {
                createCmds.add(t.createDB);
                deleteCmds.add(t.deleteDB);
            }
        }
        static SQLCollection instance = null;
        static SQLCollection i() {
            if (instance == null) {
                instance = new SQLCollection();
            }
            return instance;
        }
    }

    public class NewsDBHelper extends SQLiteOpenHelper {
        public static final int DB_VER = 1;
        public static final String DB_NAME = "news.db";

        NewsDBHelper(Context context) {
            super(context, DB_NAME, null, DB_VER);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            SQLCollection sc = SQLCollection.i();
            for (String s: sc.createCmds) {
                db.execSQL(s);
            }
        }

        void dropTables(SQLiteDatabase db) {
            SQLCollection sc = SQLCollection.i();
            for (String s: sc.deleteCmds) {
                db.execSQL(s);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            dropTables(db);
            onCreate(db);
        }
    }

}
