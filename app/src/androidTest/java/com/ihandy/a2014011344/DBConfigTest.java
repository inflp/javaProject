package com.ihandy.a2014011344;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import com.ihandy.a2014011344.NewsAPI.Category;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.ihandy.a2014011344.NewsAPI.Locale.*;
import static org.junit.Assert.*;

import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.Schedulers;

@SuppressWarnings("deprecation")

@RunWith(AndroidJUnit4.class)
public class DBConfigTest {

    DB dbInstance;
    Context context;
    static boolean firstRun = true;
    SettingsManager settingsManager;

    public DBConfigTest() {
        if (firstRun) {
            firstRun = false;
            RxJavaPlugins.getInstance().reset();
            RxJavaPlugins.getInstance().registerSchedulersHook(new RxJavaSchedulersHook() {
                @Override
                public Scheduler getIOScheduler() {
                    return Schedulers.immediate();
                }

                @Override
                public Scheduler getComputationScheduler() {
                    return Schedulers.immediate();
                }
            });
            RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
                @Override
                public Scheduler getMainThreadScheduler() {
                    return Schedulers.immediate();
                }
            });
        }
    }

    @Before
    public void setupDB() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test_");
        dbInstance = DB.i(context);
        dbInstance.dropDB().subscribe();
        settingsManager = new SettingsManager(context);
    }

    private static final String TAG = "DBBasicTest";

    private List<Category> g(String ids[]) {
        ArrayList<Category> ret = new ArrayList<>();
        for (String s: ids) {
            ret.add(new Category(s, "N" + s));
        }
        return ret;
    }
    private Set<String> s(String ids[]) {
        HashSet<String> ret = new HashSet<>();
        ret.addAll(Arrays.asList(ids));
        return ret;
    }

    @Test
    public void testDrop() {
        settingsManager.updateCategories(CN, g(new String[]{"1", "2", "3"}));
        dbInstance.dropDB().subscribe();

        assertEquals(0, settingsManager.getFilter(CN).catIds.size());

        // flush shouldn't fail
        settingsManager.updateCategories(CN, g(new String[]{"1", "2", "3"}));
    }

    @Test
    public void testRW() {
        settingsManager.updateCategories(CN, g(new String[]{"1", "2", "3"}));
        try { Thread.sleep(500); } catch (Exception e) {}
        SettingsManager.instance = null;
        dbInstance.queryDB(DB.ConfigEntry.DB_NAME, null, null)
                .subscribe(cursor -> {
                    assertEquals(1, cursor.getCount());
                    cursor.close();
                });
        assertEquals(3, SettingsManager.getInstance(context).settings.cn.catsOn.size());
    }

    @Test
    public void testMerge() {
        settingsManager.updateCategories(CN, g(new String[]{"1", "2", "3"}));
        assertEquals(3, settingsManager.getFilter(CN).catsOn.size());
        assertEquals(3, settingsManager.getFilter(CN).catIds.size());

        settingsManager.updateFilters(null, s(new String[]{"2", "4"}));
        assertEquals(1, settingsManager.getFilter(CN).catsOn.size());
        assertEquals(3, settingsManager.getFilter(CN).catIds.size());

        settingsManager.updateFilters(null, s(new String[]{"2", "1"}));
        assertEquals(2, settingsManager.getFilter(CN).catsOn.size());

        settingsManager.updateCategories(CN, g(new String[]{"3", "2", "5", "7"}));
        assertEquals(3, settingsManager.getFilter(CN).catsOn.size());
        assertEquals(4, settingsManager.getFilter(CN).catIds.size());
    }
}
