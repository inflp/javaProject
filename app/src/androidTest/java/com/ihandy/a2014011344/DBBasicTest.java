package com.ihandy.a2014011344;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.Schedulers;

class TestAPI implements NewsAPI {
    public int getCatsEmitted() {
        return catsEmitted;
    }

    public void setCatsEmitted(int catsEmitted) {
        this.catsEmitted = catsEmitted;
    }

    int catsEmitted = 10;

    public int getNewsEmitted() {
        return newsEmitted;
    }

    public void setNewsEmitted(int newsEmitted) {
        this.newsEmitted = newsEmitted;
    }

    int newsEmitted = 10;

    @Override
    public Observable<List<Category>> getCategories(String timestamp) {
        ArrayList<Category> ret = new ArrayList<>();
        for (int i = 0; i < catsEmitted; ++i) {
            ret.add(new Category("id" + i, "name" + i));
        }
        return Observable.just(ret);
    }

    @Override
    public Observable<List<News>> getNewsByCategory(String category, String max_id) {
        ArrayList<News> ret = new ArrayList<>();
        if (! category.equals("inexist")) {
            for (int i = 0; i < newsEmitted; ++i) {
                News n = new News();
                n.news_id = Integer.parseInt(category.substring(2)) * 10000 + i;
                n.category = category;
                n.title = "Title " + n.news_id;
                ret.add(n);
            }
        }
        return Observable.just(ret);
    }
}

@SuppressWarnings("deprecation")

@RunWith(AndroidJUnit4.class)
public class DBBasicTest {

    DB dbInstance;
    Context context;
    NewsAPI wAPI, rAPI;
    static boolean firstRun = true;
    private TestAPI tAPI;

    public DBBasicTest() {
        if (firstRun) {
            firstRun = false;
            RxJavaPlugins.getInstance().reset();
            RxJavaPlugins.getInstance().registerSchedulersHook(new RxJavaSchedulersHook() {
                @Override
                public Scheduler getIOScheduler() {
                    return Schedulers.immediate();
                }

                @Override
                public Scheduler getComputationScheduler() {
                    return Schedulers.immediate();
                }
            });
            RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
                @Override
                public Scheduler getMainThreadScheduler() {
                    return Schedulers.immediate();
                }
            });
        }
    }

    @Before
    public void setupDB() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test_");
        dbInstance = DB.i(context);
        tAPI = new TestAPI();
        wAPI = new CachedAPI(tAPI, NewsAPI.Locale.EN, context);
        rAPI = new DBNewsAPI(NewsAPI.Locale.EN, context);
        dbInstance.dropDB().subscribe();
    }

    private static final String TAG = "DBBasicTest";

    @Test
    public void testDrop() {
        dbInstance.dropDB().subscribe();
        assertTrue(! dbInstance.checkInitialized());
    }

    @Test
    public void testRW() {
        Log.i(TAG, "testRW: test started");
        tAPI.setCatsEmitted(10);
        wAPI.getCategories("11111111").subscribe((ret) -> {
            Log.i(TAG, "testRW: " + ret);
            assertTrue(ret.size() == 10);
        });
        rAPI.getCategories("11111111").subscribe((ret) -> {
            Log.i(TAG, "testRW: " + ret);
            assertTrue(ret.size() == 10);
        });
    }

    @Test
    public void test0Cats() throws Exception {
        tAPI.setCatsEmitted(0);
        wAPI.getCategories("").subscribe((ret) -> {
            Log.i(TAG, "testRW: " + ret);
            assertTrue(ret.size() == 0);
        });
        rAPI.getCategories("").subscribe((ret) -> {
            Log.i(TAG, "testRW: " + ret);
            assertTrue(ret.size() == 0);
        });
    }

    void testNewsByCats(int newsEmitted) throws Exception {
        tAPI.setCatsEmitted(10);
        tAPI.setNewsEmitted(newsEmitted);
        wAPI.getCategories("").subscribe();
        Log.i(TAG, "testNewsByCats: " + newsEmitted);
        wAPI.getNewsByCategory("id1", null).subscribe((ret) -> {
            Log.i(TAG, "testNewsByCats: " + ret);
            assertTrue(ret.size() == tAPI.getNewsEmitted());
            for (NewsAPI.News n: ret) {
                assertTrue(n.category.equals("id1"));
            }
        });
        rAPI.getNewsByCategory("id1", null).subscribe((ret) -> {
            Log.i(TAG, "testNewsByCats: rAPI: " + ret.size());
            assertTrue(ret.size() == tAPI.getNewsEmitted());
            for (NewsAPI.News n: ret) {
                assertTrue(n.category.equals("id1"));
            }
        });
    }

    @Test
    public void testNewsByCats() throws Exception {
        testNewsByCats(10);
        testNewsByCats(10);
    }

    @Test
    public void test0NewsByCats() throws Exception {
        testNewsByCats(0);
    }

    @Test
    public void testCachingInExistCat() {
        tAPI.setCatsEmitted(10);
        wAPI.getNewsByCategory("inexist", null).subscribe((r) -> {
            assertTrue(r.size() == 0);
        });
        rAPI.getNewsByCategory("inexist", null).subscribe((r) -> {
            assertTrue(r.size() == 0);
        });
    }

    @Test
    public void testMultiCache() throws Exception {
        testNewsByCats(10);
        tAPI.setCatsEmitted(10);
        tAPI.setNewsEmitted(5);
        wAPI.getCategories("").subscribe();
        wAPI.getNewsByCategory("id1", null).subscribe((ret) -> {
            Log.i(TAG, "testNewsByCats: " + ret);
            assertTrue(ret.size() == tAPI.getNewsEmitted());
            for (NewsAPI.News n: ret) {
                assertTrue(n.category.equals("id1"));
            }
        });
        rAPI.getNewsByCategory("id1", null).subscribe((ret) -> {
            Log.i(TAG, "testNewsByCats: rAPI: " + ret);
            assertTrue(ret.size() == 10);
            for (NewsAPI.News n: ret) {
                assertTrue(n.category.equals("id1"));
            }
        });
    }

}